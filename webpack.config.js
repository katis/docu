module.exports = {
    entry: './client-src/main.tsx',
    output: {
        filename: './src/main/resources/_static/bundle.js'
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: 'babel?presets[]=es2015!ts-loader' },
            { test: /\.json$/, loader: 'json-loader' }
        ],
        preLoaders: [
            { test: /\.js$/, loader: 'source-map-loader' }
        ]
    }
}