/// <reference path="../typings/index.d.ts" />
import React = require('react')
import ReactDOM = require('react-dom')
import {observable} from 'mobx'
import {observer} from 'mobx-react'
import {Article} from './article'
import {Index} from './index'
import {ArticleStore, IndexStore, ViewStore} from './store'
import {router, GrapnelRequest, Grapnel} from './router'

declare function require(path: string): any;
const grapnel = require('grapnel')

var root = document.getElementById('root')

function setupRouter(view: ViewStore, index: IndexStore, article: ArticleStore) {
    router.get('/*', function(req: GrapnelRequest) {
        if (req.match[0] === '/') {
            view.currentView = 'index'
            index.loadIndex()
        } else {
            view.currentView = 'article'
            article.loadMarkdown(req.params[0])           
        }
    })
}

const boundFetch = fetch.bind(this)
const viewStore = new ViewStore(),
      indexStore = new IndexStore(boundFetch),
      articleStore = new ArticleStore(boundFetch)

setupRouter(viewStore, indexStore, articleStore)

@observer
class App extends React.Component<{router: Grapnel, view: ViewStore, index: IndexStore, article: ArticleStore}, any> {
    static childContextTypes: any = {
        router: React.PropTypes.any.isRequired
    };

    getChildContext() {
        return { router: this.props.router }
    }

    render() {
        const {currentView, articlePath} = this.props.view
        switch(currentView) {
            case 'index':
                return <Index index={ this.props.index }  />
            case 'article':
                return <Article article={ this.props.article } />
        }
    }
}

ReactDOM.render(<App router={router} view={viewStore} index={indexStore} article={articleStore} />, root)