import React = require('react')
import {observable, autorun} from 'mobx'
import {observer} from 'mobx-react'
import {ArticleStore} from './store'

declare function require(path: string): any;
const ReactMarkdown = require('react-markdown')
const SyntaxHighlighter = require('react-syntax-highlighter').default

import {SequenceDiagram} from './charts'
import {DotDiagram} from './charts'

@observer
export class Article extends React.Component<{article: ArticleStore}, any> {
    constructor(props: any, ctx: any) {
        super(props, ctx)

        autorun(() => {
            if (this.props.article.markdownSource) {
                console.log(this.props.article.markdownSource.state)
            } else {
                console.log("no markdown source available")
            }
        })
    }

    render() {
        const {markdownSource} = this.props.article
        if (!markdownSource) {
            return <p>loading...</p>
        }
        switch (markdownSource.state) {
            case 'pending':
                return <p>loading...</p>
            case 'rejected':
                return <p>Failed to load article.</p>
            case 'fulfilled':
                return <ReactMarkdown source={markdownSource.value} renderers= { { 'CodeBlock': CodeBlock } } />
        }
    }
}

const graphVizPrefix = 'graphviz:'

class CodeBlock extends React.Component<{language: string, literal: string}, any> {
    render() {
        if (!this.props.hasOwnProperty('language')) {
            return <code>{this.props.literal}</code>
        }

        if (this.props.language === 'msc') {
            return <SequenceDiagram source={this.props.literal} />
        } else if (this.props.language.startsWith('graphviz')) {
            const engine = this.props.language.startsWith(graphVizPrefix) ? this.props.language.substr(graphVizPrefix.length) : 'dot'
            return <DotDiagram source={this.props.literal} engine={engine} />
        } else {
            return React.createElement(SyntaxHighlighter, { language: this.props.language }, this.props.literal)
            }
    }
}