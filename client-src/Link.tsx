import React = require('react')
import {observer} from 'mobx-react'
import {router, Grapnel} from './router'

@observer
export default class Link extends React.Component<{href: string}, any> {
    context: { router: Grapnel }

    static contextTypes: any = {
        router: React.PropTypes.any.isRequired
    }

    onClick = (ev: React.MouseEvent) => {
        const tag = (ev.nativeEvent as NavigationEvent).target as any;
        if (tag.href && ev.button === 0) {
            if (tag.origin == document.location.origin) {
                const oldPath = document.location.pathname,
                      newPath = tag.pathname
                
                ev.preventDefault()
                this.context.router.navigate(this.props.href)
            }
        }
    }

    render() {
        const {href, children} = this.props
        return (
            <a href={href} onClick={ this.onClick } >{children}</a>
        )
    }
}
