import {fromPromise as oldFromPromise, IPromiseBasedObservable} from 'mobx-utils'

// fromPromise type signature is not compatible with Promise for some reason, this hacks around it
export function fromPromise<T>(promise: Promise<T>): IPromiseBasedObservable<T> {
    const v: any = promise
    return oldFromPromise<T>(v)
}
