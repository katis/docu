declare function require(path: string): any;
const Grapnel = require('grapnel')

export interface GrapnelRequest {
    params: string[]
    match: string[]
}

export interface Grapnel {
    get(path: string, on: (request: GrapnelRequest) => any): void;
    navigate(path: string): void;
}

export const router: Grapnel = new Grapnel({ pushState: true })