import React = require('react')
import {observable} from 'mobx'
import {observer} from 'mobx-react'

declare const Viz: any
declare const mermaidAPI: any

@observer
export class DotDiagram extends React.Component<{source: string, engine: string}, any> {
    @observable svgSource: string = ""

    componentDidMount() {
        try {
            this.svgSource = Viz(this.props.source, { format: "svg", engine: this.props.engine })
        } catch (ex) {
            this.svgSource = ""
            console.log("Failed to render dot diagram", ex)
        }
    }

    render() {
        return <div dangerouslySetInnerHTML={ { __html: this.svgSource } } ></div>
    }
}

@observer
export class SequenceDiagram extends React.Component<{source: string}, any> {
    @observable svgSource: string = ""

    componentDidMount() {
        const source = "sequenceDiagram\n" + this.props.source
        mermaidAPI.render('graphDiv', source, this.updateSvgSource)
    }

    updateSvgSource = (svgCode: string) => {
        this.svgSource = svgCode
    }

    render() {
        return <div dangerouslySetInnerHTML={ { __html: this.svgSource } } ></div>
    }
}