import React = require('react')
import ReactDOM = require('react-dom')
import {observer} from 'mobx-react'
import {IPromiseBasedObservable} from 'mobx-utils'
import {ViewStore, IndexStore, IndexEntry} from './store'
import Link from './Link'

function entryElement(entry: IndexEntry) {
    return <li><Link key={ entry.path } href={ entry.path }>{ entry.name }</Link></li>
}

function indexContents(entries: IPromiseBasedObservable<IndexEntry[]>) {
    switch(entries.state) {
        case 'pending':
            return <p>loading</p>
        case 'fulfilled':
            return <ul> { entries.value.map(entryElement) } </ul>
        case 'rejected':
            return <p>Failed to load index entries</p>
    }
}

export const Index = observer<{index: IndexStore}>(store => (
    <div>
        <h1>Index</h1>
        { indexContents(store.index.entries) }
    </div>   
))

