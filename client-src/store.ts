import {action, observable} from 'mobx'
import {IPromiseBasedObservable} from 'mobx-utils'
import {fromPromise} from './mobx-utils'

export class ViewStore {
    @observable currentView: 'index'|'article' = 'index';
    @observable articlePath: string = null;
}

export class IndexStore {
    constructor(private fetch: IFetchStatic) {}

    @observable entries: IPromiseBasedObservable<IndexEntry[]> = null

    @action loadIndex() {
        this.entries = fromPromise(this.fetch(
                '/api/index',
                { headers: { 'Accept': 'application/json' } })
            .then((r) => r.text())
            .then(JSON.parse))
    }
}

export interface IndexEntry {
    name: string
    path: string
}

export class ArticleStore {
    constructor(private fetch: IFetchStatic) {}

    @observable markdownSource: IPromiseBasedObservable<string> = null

    @action loadMarkdown(path: string) {
        const response = this.fetch(
            '/files/' + path,
            { headers: { 'Accept': 'text/markdown; charset=utf-8' } })

        this.markdownSource = fromPromise(response.then((r) => r.text()))
    }
}