package org.murtsi.docu

import java.io.InputStream

inline fun <T : AutoCloseable, R> T.use(body: (T) -> R): R {
    try {
        return body(this)
    } finally {
        this.close()
    }
}

private object resourceClass {}

fun resourceStream(path: String): InputStream = resourceClass::class.java.getResourceAsStream(path)