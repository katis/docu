package org.murtsi.docu

import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.get
import java.io.File
import java.io.InputStream

fun setupRoutes() {
    fun resourceResponder(contentType: String, path: String): (Request, Response) -> InputStream =
        { request, response ->
            response.header("Content-Type", contentType)
            resourceStream(path)
        }

    val gson = Gson()
    fun json(valueFun: () -> Any) = { request: Request, response: Response -> gson.toJson(valueFun()) }

    val indexFileResponse = resourceResponder("text/html", "/_static/index.html")

    fun indexEntries() =
        matchedFilesWalk(".*\\.md$", "")
            .map { indexEntry(it.toString()) }
            .sortedBy { it.path }

    get("/bundle.js", resourceResponder("application/javascript", "/_static/bundle.js"))
    get("/bundle.js.map", resourceResponder("application/javascript", "/_static/bundle.js.map"))
    get("/viz.js", resourceResponder("application/javascript", "/_static/viz.js"))
    get("/mermaidAPI.min.js", resourceResponder("application/javascript", "/_static/mermaidAPI.min.js"))
    get("/api/index", json(::indexEntries))
    get("/files/*") { req, res -> File(req.splat()[0]).inputStream() }

    get("/", indexFileResponse)
    get("/*", indexFileResponse)
}
