package org.murtsi.docu

import spark.Spark
import java.awt.Desktop
import java.net.URI

fun main(args: Array<String>) {
    val port = 9493
    Spark.port(port)
    setupRoutes()
    Desktop.getDesktop().browse(URI("http://localhost:$port"))
}
