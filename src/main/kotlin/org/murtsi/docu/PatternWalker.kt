package org.murtsi.docu

import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

private class PatternWalker(
        pattern: String,
        val ignoredDirectories: List<String> = listOf("node_modules", "bower_components")) : SimpleFileVisitor<Path>() {
    private val patternRegex = Regex(pattern)
    private val mutableFiles = mutableListOf<Path>()
    val files: List<Path> get() = mutableFiles

    override fun visitFile(file: Path, attributes: BasicFileAttributes): FileVisitResult {
        val pathString = file.toAbsolutePath().toString()
        if (ignoredDirectories.any { pathString.contains(it) }) {
            return FileVisitResult.SKIP_SIBLINGS
        }
        if (attributes.isRegularFile || attributes.isSymbolicLink) {
            if (patternRegex.matches(file.toAbsolutePath().toString())) {
                mutableFiles.add(file)
            }
        }

        return FileVisitResult.CONTINUE
    }
}

fun matchedFilesWalk(pattern: String, rootPath: String): List<Path> {
    val walker = PatternWalker(pattern)
    Files.walkFileTree(Paths.get(rootPath), walker)
    return walker.files
}
