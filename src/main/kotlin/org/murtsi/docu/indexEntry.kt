package org.murtsi.docu

import java.nio.file.Files
import java.nio.file.Paths

data class IndexEntry(val name: String, val path: String)

fun indexEntry(path: String): IndexEntry {
    fun String.stripHeader(): String {
        val endIndex = this.toCharArray().indexOfFirst { it == '[' || it == '!' || it == '(' }
        return if (endIndex == -1) this else this.substring(0, endIndex)
    }

    val header = Files.lines(Paths.get(path)).use {
        it.map { it.trim() }
                .filter { it.startsWith("# ") }
                .map { it.removePrefix("# ").stripHeader().trim() }
                .findFirst()
    }

    return IndexEntry(header.orElse(path), "/$path")
}
