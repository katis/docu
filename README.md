# Docu

Docu is a simple markdown documentation renderer for your local computer.
It supports syntax highlighting in code blocks and rendering charts using DSLs.

A compiled .jar can be downloaded [here](https://bitbucket.org/katis/docu/downloads).

![test image](/files/img.png "Logo Title Text 1")

## Charts DSLs

Open this file in docu to view the charts properly.

### Message sequence chart

Using `msc` as a language in a code block tries to render it's contents using the syntax of [Mermaid](https://knsv.github.io/mermaid/#syntax).

```msc
Alice->>John: Hello John, how are you?
John-->>Alice: Great!
```

### Graphviz

Using `graphviz:<engine>` as a code block language the contents is rendered as a Graphviz-graph with the specified engine.

See http://www.tonyballantyne.com/graphs.html for examples.

```graphviz:dot
digraph G {

    subgraph cluster_0 {
        style=filled;
        color=lightgrey;
        node [style=filled,color=white];
        a0 -> a1 -> a2 -> a3;
        label = "process #1";
    }

    subgraph cluster_1 {
        node [style=filled];
        b0 -> b1 -> b2 -> b3;
        label = "process #2";
        color=blue
    }
    start -> a0;
    start -> b0;
    a1 -> b3;
    b2 -> a3;
    a3 -> a0;
    a3 -> end;
    b3 -> end;

    start [shape=Mdiamond];
    end [shape=Msquare];
}
```